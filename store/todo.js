export const state = () => ({
    // list: ['it', 'just', 'dummy', 'content']
    list: []
})


export const mutations = {
    add(state, text) {
        state.list.push({
            text,
            done: false
        })
        // console.log('Hai, ini dari mutation')
        // console.log(state)
    },

    remove(state, todo) {
        state.list.splice(state.list.indexOf(todo), 1)
    },

    toggle(state, todo) {
        // console.log('toggle method')
        // console.log(todo)
        
        todo.done = !todo.done
    }
}