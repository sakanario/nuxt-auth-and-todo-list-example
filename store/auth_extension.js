export const getters = {
    isAuthenticated(state,getters,rootState) {
        // console.log(rootState)
        return rootState.auth.loggedIn
    },

    loggedInUser(state,getters,rootState) {
        // console.log("Ini dari auth js")
        // console.log(state)
        return rootState.auth.user
    },

    test(state,getters,rootState) {
        console.log("auth/test")
        return rootState
    }
}