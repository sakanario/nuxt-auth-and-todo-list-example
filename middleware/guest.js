export default function ({ store, redirect }) {
    if (store.state.auth.loggedIn) {
        return redirect('/')
    }

    // if (true) {
    //     return redirect('/')
    // }

    // if (this.$auth.loggedIn) {
    //     return redirect('/')
    // }
} 